Rule:
Flutter projects must include assets (sometimes called resources). An asset is a directory that
is bundled and deployed with your app, and is accessible at runtime. Common types of assets include
configuration files, fonts, text styles, colors, gradients, shadows, icons, and images (JPEG, WebP, GIF, animated WebP/GIF, PNG, BMP, and WBMP).
Access to this file must be implemented through classes that include static values with asset files used in project!
 
Why:
All the assets in a single place - so we can see and control them all at once. We have less chances to make mistakes! 
