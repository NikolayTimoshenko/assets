import 'package:assets/res/app_style/app_colors.dart';
import 'package:assets/res/app_style/app_gradients.dart';
import 'package:assets/res/app_style/app_shadows.dart';
import 'package:assets/res/assets/icons/update_icon.dart';
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final String title;
  final void Function() onTap;
  final TextStyle textStyle;

  const AppButton({
    @required this.title,
    @required this.onTap,
    @required this.textStyle,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: AppColors.transparent,
      highlightColor: AppColors.transparent,
      onTap: onTap,
      child: Container(
        height: 50.0,
        margin: const EdgeInsets.symmetric(horizontal: 20.0),
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
          gradient: AppGradient.mainGradient,
          boxShadow: AppShadows.globalAppButton(
            color: AppColors.lightBlue.withOpacity(0.3),
          ),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text(
              title,
              style: textStyle,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Icon(
                  UpdateIcon.update_icon,
                  size: 30.0,
                  color: AppColors.white,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
