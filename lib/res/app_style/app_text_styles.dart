import 'package:assets/res/app_style/app_colors.dart';
import 'package:flutter/material.dart';

class AppTextStyles {
  static const TextStyle s27fw700Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    color: AppColors.black,
    fontSize: 27.0,
    height: 1.0,
    fontWeight: FontWeight.w500,
  );
  static const TextStyle s12fw500Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    color: AppColors.darkSlateBlue,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s15fw300Montserrat = TextStyle(
    fontFamily: 'Montserrat',
    color: AppColors.white,
    fontSize: 15.0,
    height: 22.0 / 15.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s27fw700RobotoMono = TextStyle(
    fontFamily: 'RobotoMono',
    color: AppColors.black,
    fontSize: 27.0,
    height: 1.0,
    fontWeight: FontWeight.w500,
  );
  static const TextStyle s12fw500RobotoMono = TextStyle(
    fontFamily: 'RobotoMono',
    color: AppColors.darkSlateBlue,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s15fw300RobotoMono = TextStyle(
    fontFamily: 'RobotoMono',
    color: AppColors.white,
    fontSize: 15.0,
    height: 22.0 / 15.0,
    fontWeight: FontWeight.w500,
  );
}
