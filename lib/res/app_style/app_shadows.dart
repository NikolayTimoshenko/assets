import 'package:assets/res/app_style/app_colors.dart';
import 'package:flutter/material.dart';

class AppShadows {
  static List<BoxShadow> globalAppButton({Color color}) {
    return [
      BoxShadow(
        color: color ?? AppColors.chathamsBlue.withOpacity(0.1),
        offset: Offset(0.0, 10.0),
        blurRadius: 35.0,
      ),
    ];
  }
}
