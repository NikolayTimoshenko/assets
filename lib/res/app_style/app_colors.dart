import 'package:flutter/material.dart';

class AppColors {
  static const Color transparent = Color(0x00000000);
  static const Color aliceBlue = Color(0xFFF2F6FE);
  static const Color aliceBlue2 = Color(0xFFf2f6fe);
  static const Color topaz = Color(0xFF1ACCAE);

  static const Color pinkyLight = Color(0xFFE0A8EA);
  static const Color mainPurple = Color(0xFF6B39B6);
  static const Color mainPink = Color(0xFFA44DB3);

  static const Color thunder = Color(0xFF303030);
  static const Color darkSlateBlue = Color(0xFF1d3557);
  static const Color chathamsBlue = Color(0xFF234b70);
  static const Color greenWhite = Color(0xFFe8e8e8);
  static const Color lipstick = Color(0xFFE63946);
  static const Color jasmine = Color(0xFFFFDC7E);
  static const Color lightBlue = Color(0xFF62aef5);
  static const Color veryLightPink = Color(0xFFfef2f2);
  static const Color shadowRed = Color(0xFFed8882);

  static const Color black = Color(0xFF1D1626);
  static const Color white = Color(0xFFFFFFFF);
  static const Color whiteTwo = Color(0xFFd8d8d8);
  static const Color whiteThree = Color(0xFFf0f0f0);
  static const Color background = Color(0xFFF2F2F2);
  static const Color grey = Color(0xFF8E8B93);
  static const Color pinkishGrey = Color(0xFFD0D0D0);
  static const Color greyTwo = Color(0xFFD2D0D4);
  static const Color darkGrey = Color(0xFF3C3C43);
  static const Color lightGrey = Color(0xFF828282);
  static const Color pinkSwan = Color(0xFFb2b2b2);
  static const Color borderColor = Color(0xFFf0f0f0);

  static const Color success = Color(0xFF55CF70);
  static const Color error = Color(0xFFF44336);

  static const Color stars = Color(0xFFF0CA90);
}