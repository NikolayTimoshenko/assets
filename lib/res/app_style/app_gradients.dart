import 'package:assets/res/app_style/app_colors.dart';
import 'package:flutter/material.dart';

class AppGradient {
  static LinearGradient get mainGradient {
    return LinearGradient(
      begin: Alignment.bottomLeft,
      end: Alignment.topRight,
      colors: [
        AppColors.jasmine,
        AppColors.lipstick,
      ],
    );
  }
}
