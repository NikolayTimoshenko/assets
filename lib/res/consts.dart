class AppConsts {
  static const String appVestoText = 'AppVesto';
  static const String changeBackgroundColor = 'Change background color';
  static const String changeTextStyles = 'Change text styles';
}
