/// [ImageAssets] This class contains constant values with asset images used in application.

class AppImages {
  static const String appVestoLogo = 'assets/png/app_vesto_logo.png';
}
