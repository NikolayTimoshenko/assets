import 'package:assets/res/app_style/app_colors.dart';
import 'package:assets/res/app_style/app_text_styles.dart';
import 'package:assets/res/assets/app_images.dart';
import 'package:assets/res/consts.dart';
import 'package:assets/widgets/app_button.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _changeBackgroundColor = true;
  bool _changeTextStyle = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: _changeBackgroundColor ? AppColors.white : AppColors.pinkyLight,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 100.0,
                height: 100.0,
                child: Image.asset(
                  AppImages.appVestoLogo,
                ),
              ),
              const SizedBox(height: 50.0),
              Text(
                AppConsts.appVestoText,
                style: _changeTextStyle ? AppTextStyles.s27fw700Montserrat : AppTextStyles.s27fw700RobotoMono,
              ),
              const SizedBox(height: 50.0),
              AppButton(
                title: AppConsts.changeBackgroundColor,
                onTap: () {
                  setState(() {
                    _changeBackgroundColor = !_changeBackgroundColor;
                  });
                },
                textStyle: _changeTextStyle ? AppTextStyles.s15fw300Montserrat : AppTextStyles.s15fw300RobotoMono,
              ),
              const SizedBox(height: 20.0),
              AppButton(
                title: AppConsts.changeTextStyles,
                onTap: () {
                  setState(() {
                    _changeTextStyle = !_changeTextStyle;
                  });
                },
                textStyle: _changeTextStyle ? AppTextStyles.s15fw300Montserrat : AppTextStyles.s15fw300RobotoMono,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
